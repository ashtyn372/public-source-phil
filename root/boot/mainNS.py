#! /usr/bin/env python3.7
 #import colorama
from colorama import *
init(autoreset=True)
def mainNS():

    import atexit, os
    os.system("stty eof ^X")
    atexit.register(lambda: os.system("stty eof ^D"))

    import sys
    import os
    from pathlib import Path
    my_path = os.path.abspath(os.path.dirname(__file__))
    path = os.path.join(my_path, "../bin")
    sys.path.insert(0, path)
    import configReader
    my_path = os.path.abspath(os.path.dirname(__file__))
    path = os.path.join(my_path, "../lib")
    sys.path.insert(0, path)
    import logger
    logger.InLog("sys:successfully loaded boot")
    #checks usr for user dir
    my_path = os.path.abspath(os.path.dirname(__file__))
    path = os.path.join(my_path, "../lib")
    sys.path.insert(0, path)

    import local
    local.usrcheck()
    if 'boot-state=True' not in local.check.read():
        print("sys:booting into img LOADER")
        logger.InsysLog("No boot state DEFINED\nResolve:booting into img-build")
        local.check.write("boot-state=True")
        os.system("~/master/root/xf10/__img-builder/./set.py")
    else:
        print("status: True")

    print("")
    print("welcome")

    def passreset(secret2):
            filehandle2 = open('../etc/4532/45.txt', 'w')
            filehandle2.write('')
            filehandle2.write(secret2)
            filehandle2.close()

    while True:
        try:
            my_path = os.path.abspath(os.path.dirname(__file__))
            path = os.path.join(my_path, "../lib")
            sys.path.insert(0, path)
            import local
            username = local.username
            if configReader.prompt == "defualt":
                pointer = input("["+username+"@"+os.path.basename(os.getcwd())+"]"+"-> ")
            elif configReader.prompt != "defualt":
                pointer = input(configReader.prompt)
            if pointer == "user":
                class user:
                    current = os.getcwd()
                    name2 = input('what is your user name:')
                    password = input('what is your password:')
                    import local
                    local.namestore(name2)
                    local.passstore(password)
                    print(name2)
                    logger.InLog("sys:new user created "+name2)
                    print("type cd to navigate to home")
                    os.system("~/master/root/boot/./boot-loaders.py")
                    sys.exit()


                print("account information was made!")

            elif pointer == "sudo -reset":
                key = open('etc/4532/45.txt', 'r').read()
                if key == '':
                    print("you have not made an account")
                else:
                    password = input("resest password>")
                    print("password was switched!")

            elif "pack-get" in pointer:
                get = pointer[9::]
                my_path = os.path.abspath(os.path.dirname(__file__))
                path = os.path.join(my_path, "../bin/")
                sys.path.insert(0 ,path)
                import pack
                pack.pack(get)

            elif pointer == "time":
                my_path = os.path.abspath(os.path.dirname(__file__))
                path = os.path.join(my_path, "../bin/")
                sys.path.insert(0,path)
                import pack
                pack.pack("time")

            elif pointer == "net":
                my_path = os.path.abspath(os.path.dirname(__file__))
                path = os.path.join(my_path, "../lib/local.py")
                sys.path.insert(0, path)
                import local
                local.netget()

            elif pointer == "lusb":
                local.usbget()

            elif pointer == "--v":
                print("VERSION: dev 2.3.4")

            elif "clear" in pointer or "cls" in pointer:
                os.system("clear")

            elif "md" in pointer:
                name = pointer[3::]
                os.system("mkdir "+name)

            elif 'rmD' in pointer:
                name2 = pointer[4::]
                check2 = os.path.isdir(name2)
                if check2 == True:
                    os.system("rm -r " + name2)
                    print(name2 + " was deleted")
                else:
                    print("NO DIRECTORY FOUND")

            elif 'cd' in pointer:
                name = pointer[3::]
                if os.path.isdir(name) == True:
                    os.chdir(name)
                elif name == '':
                    os.chdir(local.home+"/"+username)
                else:
                    print("DIRECTORY NOT FOUND")

            elif pointer == 'lf' or pointer ==  'lf -d' or pointer == 'lf -f':
                cwd = os.getcwd()
                #if folder doesn't have contents
                cd = os.listdir(cwd)
                filenames = [f for f in cd if os.path.isfile(os.path.join(cwd, f))]
                dirnames = [d for d in cd if os.path.isdir(os.path.join(cwd, d))]
                length = len(cd)
                if pointer == 'lf -d':
                    print(*dirnames)
                elif pointer == 'lf -f':
                    print(*filenames)
                else:
                    if length == 0:
                        print("this directory is empty")
                    else:
                        #if the folder has contents
                        files = os.listdir(cwd)
                        if True==True:
                            print(*filenames, sep="\n")
                            for i in dirnames:
                                print(Fore.CYAN+Style.BRIGHT+i)

            elif 'lF' in pointer:
                ext = os.listdir(pointer[3::])
                print(*ext)

            elif 'make' in pointer:
                nameType = pointer[5::]
                if os.path.isfile(nameType) == True:
                    print("can not make file, already exists")
                else:
                    if nameType == '':
                        print("missing oprand")
                    else:
                        if "img" in nameType:
                            print("file made was a system resource!")
                            continuer = input("are you sure you want to make a system resource[y/n] ")
                            if continuer == "y" :
                                print("The file was moved to boot/PHI/ and was renamed as a resource")
                                os.system("touch ~/master/root/boot/PHI/*.PHI")
                            else:
                                print("discarding file...")
                        else:
                            os.system("touch " + nameType)
            elif 'rep' in pointer or 'rep -s' in pointer:
                if 'rep -s' in pointer:
                    filename = pointer[7::]
                    print(filename + "\nplaying...")
                    import pygame
                    pygame.init()
                    pygame.mixer.init()
                    pygame.mixer.music.load(filename)
                    pygame.mixer.music.play()
                    os.system('clear')
                    while pygame.mixer.music.get_busy() == True:
                        op = input('options: play, pause, exit> ')
                        if op == "pause":
                            pygame.mixer.music.pause()
                        if op == "exit":
                            print("\n")
                            pygame.mixer.music.stop()
                        if op == "play":
                            pygame.mixer.music.unpause()

                else:
                    text = pointer[4::]
                    print(text)

            elif 'REP -s' in pointer:
                 import pygame
                 filenames = pointer[7::]
                 playlist = filenames.split(',')
                 def play(listing):
                     print(i," is now playing...")
                     pygame.mixer.init()
                     pygame.mixer.music.load(listing)
                     pygame.mixer.music.play()
                     os.system('clear')
                 for i in playlist:
                     play(i)
                 while pygame.mixer.music.get_busy() == True:
                     op = input('options: play, pause, skip, exit> ')
                     if op == "pause":
                         pygame.mixer.music.pause()
                     if op == "skip":
                         pygame.mixer.music.stop()
                     if op == "play":
                         pygame.mixer.music.unpause()
                     if op == "exit":
                         print("\n")
                         pygame.mixer.music.stop()
                         break


            elif 'show' in pointer:
                filename = pointer[5::]
                check = os.path.isfile(filename)
                if check == True:
                    printer = open(filename, "r").read()
                    os.system("clear")
                    print(printer)
                    continuer = input("press enter to continue...")
                    if continuer == '':
                        os.system("clear")

                elif check == False:
                    print("file not found")

            elif 'rm' in pointer:
                filename2 = pointer[3::]
                check2 = os.path.isfile(filename2)
                if check2 == True:
                    os.system("rm "+filename2)
                elif check2 == False:
                    print("FILE NOT FOUND")
            elif 'mv' in pointer or 'move' in pointer:
                import shutil
                des = pointer[3::].split(':')
                check4 = os.path.isdir(des[0])
                check5 = os.path.isfile(des[0])
                try:
                    if check4 == True or check5 == True:
                        shutil.move(des[0], des[1])
                    elif des[0] == "" or des[0] == " " or des[0] == "" or des[1] == " ":
                        print("missing operand or could not find file/dir")
                except IndexError:
                    print("missing operand")

            elif 'unmount' in pointer:
                feilds = pointer[8::].split("-")
                try:
                    if feilds[1] == "boot":
                        os.system("clear")
                        os.system("~/master/root/boot/./NOconf.py")
                        sys.exit()
                    elif feilds[1] == "exit":
                        os.system("clear")
                        os.system("echo exit status = 1")
                        break
                        os.system("pkill python")
                except IndexError:
                    print("missing operand or False operand")
                else:
                    print("unmount "+pointer[8::]+" NOT FOUND")

            elif pointer == 'geth':
                print("usr/"+username)

            elif pointer == 'getBASE':
                base = os.path.basename(os.getcwd())
                print(base)
            elif pointer == 'getCD':
                cwd = os.getcwd()
                cd = cwd.split("/")
                print("/".join(cd[4:]))

            elif 'beth' in pointer or pointer == 'beth -nc' or pointer == 'beth -help':
                if pointer == "beth -nc":
                    os.system(" ~/master/root/bin/./non-curses-beth.py")
                    os.system("clear")
                elif pointer == 'beth -help':
                    print("Beth editor Help:\n\n\tbeth -nc:opens graphical version of beth!does not take any arguements!(requires python3.5 and tkinter)\n\n\tbeth:opens defualt curses type beth!does require arguements!(already installed if install ran)")
                else:
                    if pointer[5::] == '':
                        os.system("~/master/root/bin/./txt.py unamed")
                    else:
                        os.system("~/master/root/bin/./txt.py "+pointer[5::])
                    if KeyboardInterrupt:
                        os.system('clear')
                        print('beth exited')
            elif pointer == 'help -chrome':
                my_path = os.path.abspath(os.path.dirname(__file__))
                path = os.path.join(my_path, "../bin/")
                sys.path.insert(0 ,path)
                import pack
                pack.pack('boot_out')

            elif pointer == '':
                newline = True
            else:
                my_path = os.path.abspath(os.path.dirname(__file__))
                path = os.path.join(my_path, "../lib/")
                sys.path.insert(0 ,path)
                import logger
                print(pointer+" NOT FOUND")
                logger.InLog("failed to run: "+pointer)
        except:
            print("\nsys:error was found")
            print("can not umount kernel")
            #print(e)
            my_path = os.path.abspath(os.path.dirname(__file__))
            path = os.path.join(my_path, "../lib")
            sys.path.insert(0, path)
            import logger
            logger.InLog("sys:tried defualt termination(!can not execute")
