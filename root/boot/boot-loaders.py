#! /usr/bin/env python3.7
import os
import sys
import subprocess
my_path = os.path.abspath(os.path.dirname(__file__))                                                                                                                                                                      
path = os.path.join(my_path, "../bin")                                                                                                                                                                                    
sys.path.insert(0, path)                                                                                                                                                                                                  
import configReader as reader
reader.read()
sys = subprocess.check_output(["uname", "-v"])
sys_info = sys.decode('utf-8')
print("BOOTING\nfrom "+sys_info.split(" ")[2],' ',sys_info.split(" ")[3])
print("loading..........................................................")
print("============options for boot==============")
print("            main os shell[1]              ")
print("\n")
while True:
    try:
        pointer = input("booting>")
        if 'help' in pointer:
            print("pick a os to boot into.")

        elif '1' in pointer:
            import mainNS
            mainNS.mainNS()
            continuer = open("../var/log", "a")
            continuer.write("sys:succsessfully booted "+pointer+"\n")
            continuer.close()
        elif pointer == '':
            newline = True
        else:
            print("err thrown: could not find "+pointer)
            err = open("../var/log", "a")
            err.write("sys:error thrown "+pointer+'\n')
            err.close()
    except Exception as e:
        print('warning: log not found\n',e)        
