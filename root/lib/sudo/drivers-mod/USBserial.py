from __future__ import print_function
def usb():    
    import argparse
    import sys
    import usb
    import usb.core
    import sys
    VID = 0x04e8
    PID = 0x6860
    
    import usb.core
    import usb.backend.libusb1
    import usb.util
                    
    busses = usb.busses()
    for bus in busses:
        devices = bus.devices
        for dev in devices:
            print("Device:", dev.filename)
            print("  VID: 0x{:04x}".format(dev.idVendor))
            print("  PID: 0x{:04x}".format(dev.idProduct))
            #print("  Manufacturer: 0x{:x}".format(dev.iManufacturer), )
            dev = usb.core.find(idVendor=dev.idVendor, idProduct=dev.idProduct) # fill in your own device, of course
            if dev is None:
                print('\nOur device is not connected')
            else:
                if dev._manufacturer is None:
                    dev._manufacturer = usb.util.get_string(dev, dev.iManufacturer)
                    print("  manufacturer: ",str(dev._manufacturer))
                if dev._product is None:
                    dev._product = usb.util.get_string(dev, dev.iProduct)
                    print("  product: ",dev._product)                            
                print("  serial: ", usb.util.get_string( dev, dev.iSerialNumber ))
    