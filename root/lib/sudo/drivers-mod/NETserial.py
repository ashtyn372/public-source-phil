def interfaceloop():
    from netifaces import interfaces, ifaddresses, AF_INET
    for ifaceName in interfaces():
        addresses = [i['addr'] for i in ifaddresses(ifaceName).setdefault(AF_INET, [{'addr':'No IP addr'}] )]
        print("found: %s: found:%s' " % (ifaceName, ', '.join(addresses)))
    
def interfaceDef():
    try:
        from urllib.request import urlopen
        import re
        data = str(urlopen('http://checkip.dyndns.com/').read())
        # data = '<html><head><title>Current IP Check</title></head><body>Current IP Address: 65.96.168.198</body></html>\r\n'

        print(re.compile(r'Address: (\d+\.\d+\.\d+\.\d+)').search(data).group(1))
    except Exception:
        print("No network")
