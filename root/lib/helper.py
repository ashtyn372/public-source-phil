def proc():
    import os
    def start_seq():
        os.system('python3.7 ~/master/root/srv/PhilSite/manage.py runserver')
    def end_seq():
        import webbrowser
        webbrowser.open_new('http://localhost:8000/')
    
    from threading import Thread
    
    Thread(target = start_seq).start()
    import time
    time.sleep(2)
    Thread(target = end_seq).start()
    time.sleep(5)
    os.system('clear')
    input("exit...")
    os.system('pkill chromium')
    print('push ctrl+c to exit')    
    
