from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='archive-home'),
    path('bash.html/', views.installer, name="archive-bash-install"),
    path('docs-des.html/', views.docs, name="archive-documentation-doc"),
    path('main-private.html/', views.private, name="archive-dev-main"),
    path('conf.html/', views.conf, name="archive-conf-doc"),
    path('dev-config.html/', views.config, name="archive-dev-config"),
    path('docs.html/', views.doc, name="archive-docs-doc"),
    path('local-system.html/', views.local, name="archive-local-sys")
]
