from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def home(request):
    return render(request, 'archive/home.html')

def installer(request):
    return render(request, 'archive/bash.html')
def docs(request):
    return render(request, 'archive/docs-des.html')
def private(request):
    return render(request, 'archive/main-private.html')
def conf(request):
    return render(request, 'archive/conf.html')
def config(request):
    return render(request, 'archive/dev-config.html')
def doc(request):
    return render(request, 'archive/docs.html')
def local(request):
    return render(request, 'archive/local-system.html')
