# Auto install from bash
**zero dependencies install for Phil dependencies**

---
**warning:** This script is not supported on Mac

This script is to auto install all dependencies of Phil without
python the use of python run the executable as superuser or
know the superuser password because you will be  
prompted for root password. Use the following:
```bash
./auto [LOG FILENAME]
#or if you do not want to be prompted
sudo ./auto [LOG FILENAME]
```

### Bug fixing

  * if you get the following error:
        ```bash
             ERROR:please specify a log file
             RESOLVE:log file name
        ```
     It will prompt you to specify a log file to write to. All you need to is write the name of the file and push enter.  
     Too avoid this write the name of the log file as the first arguement!


   * If you are not superuser or do not know sudo password **this installation will not work**
