# python-shell-os
**this repo is only intended for the download & distribution of Phil source**

---
### install 
Before starting rename the directory where the git repo is stored by running:

```bash
cd ~ # make sure it's in your home dir
mv python-shell-os master #if clone dir is not named master
cd master
```
This is how the root directory is found when you boot it uses the path  
*~/master/root/usr/[username]*.

You can use the python install file which is  located master/ or   
python-shell-os if you just cloned into the repository. By running  
the following command:  

```bash
./install -auto -install -o [LOG FILENAME]
``` 
Or if python is not installed go into the RAW folder and execute:
```bash
cd RAW #to move into Raw file 

./auto [LOG FILENAME]
```
The auto script uses bash to install all of the dependencies. both  
scripts take about 5-10 minutes depending on network speed.

### support 

As of version 2.3.3 Phil is not supported for Windows.  
Phil was built around linux and Unix like operating systems and   
was ment to be booted from linux not Windows. Mac is supported   
except for the the package control methods in the bash and python  
installer. 

|**suported**|**half supported**|**Not supported**|
|------------|-------------------|----------------|
|Ubuntu	     | Mac               | Windows Cmd    | 
|Mint	       | Fedora            | 	         
|Debian	     | Red hat           |             
|	           | Arch              |             


### license agrements
all files in python-shell-os are under the APACHE 2.0   
license and you are required to followed the license rules.
