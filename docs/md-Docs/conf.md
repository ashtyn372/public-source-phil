# .conf file
---
published: 9/12/19
added: 9/12/19
owner: ashtyn372@gmail.com

The .conf has two main categories "sys:",
and "config:" they both have the same general purpose
one sys is used to write information about the operating
system directly. It is not used to set defaults for booting
such as auto boot into kernel instead of going through manual boot.
the Config is used to set defaults about the look of your
environment. One of the main functions of config is to enable
268-bit color mode, this can be used to always boot into 256-
bit color mode.
The sys category can act like a image for other Unix
and non Unix operating system(OS). you can also use conditional
statements to make the same function as your .bash-profile or to customize your OS depending on the user. In the future you will be able to more easily target specific user spaces with sys and conf.
