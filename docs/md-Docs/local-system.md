# local-system A
---
published: 9/15/19
added:9/15/19
owner:ashtyn372@gmail.com               

In version 2.3.0 of Phil the local.py file was
introduced the main goal of this file is to link all
external resources to one file. The use of linking all
files is into one local file is that you are able to
have relative file paths that are more reliable to use
when calling other resources.
For example in the configReader file in the bin
directory, that is linked to the local file to grab the
.conf file. In short everything goes through local to get
files with a relative path.                                                                                                                                
