# developer environment configuration
---
published: 9/13/19
added: 9/13/19
owner: ashtyn372@gmail.com

The Dev-config system uses your ~/.bash_profile to
make custom tools for Phil developing. These tools in general
make the Phil development life easier and sets up your clone
of the development repository for you. So you can test, edit,
and push faster.
The 2 main tools you'll be using are Phil-console
which, gives you access to the Phil tool software dev-
elopment console where you can use all of the Phil-tools(
or you can run the tools outside of Phil-console). Next
is the Phil-push command this will better automate the
add, commit, pull, push process so you can stay up to
date while being able to push your own code to the main
repo. The Phil-push works best if you have pass phrases
set up in your git account. Pass phrases will allow you
to enter in a special string(like a password) that will
unlock you account. So the push and pull don't always re-
quire you to log-in whenever you do decide to run
Phil-push.
After you run the config.py  in the dev-Config
folder you will need to restart your cli(command line enviroment)
to apply changes from the bash_profile. So don't be surprised
if the commands don't work right away.
The installing process for the tools is really simple
and will guide you through. Joe is the recommended editor
for the work flow of Phil development. You can either install
Joe or use your own personal favorite editor(which ever one
works best for you)
## PREREQUISITES
---
Before you can do any programming with your new
setup workspace you will have to install git in your cli(
command line interface) you will also obviously need pyth-
on3 to run any of the code in Phil. For the best use of config
you will most likely want to be using a linux enviroment. most
of the operating system is meant to mount on to linux or unix
like kernels and operating systems.
Another thing you will need to do before you start
working is to enable less secure app access to your account
so your changes can be emailed to you and you can be notified
about anything new. The dev-config script notifies owner by
sending a email from you to owner to let the owner know you
have joined the project as dev. All personal passwords are
only stored temporarily. **Your personal data will never be put in public or in risk of being found. All personal data you enter will be wiped from your hard drive after you finish the process that requires it.**
